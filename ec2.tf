data "aws_ami" "lblock"{
  most_recent = true
  owners = ["amazon"]

  filter{
    name="name"
    values= ["amzn*"]
    }
}

resource "aws_instance" "lec2"{
  instance_type = "t2.micro"
  ami = data.aws_ami.lblock.id
}
